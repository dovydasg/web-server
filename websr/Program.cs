﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace websr
{
    class Program
    {
        private static TcpListener _myListener;
        private static readonly int PORT = 5050;

        static void Main()
        {
            try
            {
                _myListener = new TcpListener(IPAddress.Any, PORT);
                _myListener.Start();
                Console.WriteLine("Running...");

                //allows mutiple clients
                Thread th = new Thread(Start);
                th.Start();
            }
            catch (Exception e)
            {
                Console.ReadLine();
                ExceptionLogger.Log(e);
            }
        }

        private static void SendHeader(string version, string contentType, int contentLength, string statusCode, ref Socket mySocket)
        {

            string sBuffer = "";

            if (contentType.Length == 0)
            {
                contentType = "text/html";
            }

            sBuffer = sBuffer + version + statusCode + "\r\n";
            sBuffer = sBuffer + "Server: cx1193719-b\r\n";
            sBuffer = sBuffer + "Content-Type: " + contentType + "\r\n";
            sBuffer = sBuffer + "Accept-Ranges: bytes\r\n";
            sBuffer = sBuffer + "Content-Length: " + contentLength + "\r\n\r\n";

            byte[] bSendData = Encoding.ASCII.GetBytes(sBuffer);

            SendToBrowser(bSendData, ref mySocket);
        }

        private static void SendToBrowser(string data, ref Socket mySocket)
        {
            SendToBrowser(Encoding.ASCII.GetBytes(data), ref mySocket);
        }

        private static void SendToBrowser(byte[] sendDataBytes, ref Socket mySocket)
        {
            try
            {
                if (mySocket.Connected)
                {
                    int numBytes;
                    if ((numBytes = mySocket.Send(sendDataBytes, sendDataBytes.Length, 0)) == -1)
                    {
                        Console.WriteLine("Cannot Send Packet");
                    }
                    else
                    {
                        Console.WriteLine("Bytes send {0}", numBytes);
                    }
                }
                else
                    Console.WriteLine("Connection Dropped....");
            }
            catch (Exception e)
            {
                ExceptionLogger.Log(e);
            }
        }

        private static void Start()
        {
            while (true)
            {
                try
                {
                    Socket mySocket = _myListener.AcceptSocket();

                    if (mySocket.Connected)
                    {
                        byte[] bReceive = new byte[1024];
                        mySocket.Receive(bReceive, bReceive.Length, 0);

                        string sBuffer = Encoding.ASCII.GetString(bReceive);

                        if (sBuffer.Substring(0, 3) != "GET")
                        {
                            Console.WriteLine("Only GET is allowed");
                            mySocket.Close();
                            return;
                        }

                        var iStartPos = sBuffer.IndexOf("HTTP", 1, StringComparison.Ordinal);

                        string sHttpVersion = sBuffer.Substring(iStartPos, 8);

                        var message = $"<H2>Response from server</H2><H1>{sBuffer}</H1>";
                        SendHeader(sHttpVersion, "", message.Length, "200", ref mySocket);
                        SendToBrowser(message, ref mySocket);

                        mySocket.Close();
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.Log(ex);
                    break;
                }
                
            }
        }
    }
}
